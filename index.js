const card = document.querySelector("#hidden")
const button = document.querySelector("button")
const symbolCard = document.querySelector(".color>img")
const numberCard = document.querySelector(".color>p")
const list = document.querySelector(".list")
let usedSpells = []

const availableCards =[
    {color:"./images/carreau.png",number:2},
    {color:"./images/carreau.png",number:3},
    {color:"./images/carreau.png",number:4},
    {color:"./images/carreau.png",number:5},
    {color:"./images/carreau.png",number:6},
    {color:"./images/carreau.png",number:7},
    {color:"./images/carreau.png",number:8},
    {color:"./images/carreau.png",number:9},
    {color:"./images/carreau.png",number:10},
    {color:"./images/carreau.png",number:"V"},
    {color:"./images/carreau.png",number:"D"},
    {color:"./images/carreau.png",number:"K"},
    {color:"./images/carreau.png",number:"A"},
    {color:"./images/coeur.png",number:2},
    {color:"./images/coeur.png",number:3},
    {color:"./images/coeur.png",number:4},
    {color:"./images/coeur.png",number:5},
    {color:"./images/coeur.png",number:6},
    {color:"./images/coeur.png",number:7},
    {color:"./images/coeur.png",number:8},
    {color:"./images/coeur.png",number:9},
    {color:"./images/coeur.png",number:10},
    {color:"./images/coeur.png",number:"V"},
    {color:"./images/coeur.png",number:"D"},
    {color:"./images/coeur.png",number:"K"},
    {color:"./images/coeur.png",number:"A"},
    {color:"./images/trefle.png",number:2},
    {color:"./images/trefle.png",number:3},
    {color:"./images/trefle.png",number:4},
    {color:"./images/trefle.png",number:5},
    {color:"./images/trefle.png",number:6},
    {color:"./images/trefle.png",number:7},
    {color:"./images/trefle.png",number:8},
    {color:"./images/trefle.png",number:9},
    {color:"./images/trefle.png",number:10},
    {color:"./images/trefle.png",number:"V"},
    {color:"./images/trefle.png",number:"D"},
    {color:"./images/trefle.png",number:"K"},
    {color:"./images/trefle.png",number:"A"},
    {color:"./images/pique.png",number:2},
    {color:"./images/pique.png",number:3},
    {color:"./images/pique.png",number:4},
    {color:"./images/pique.png",number:5},
    {color:"./images/pique.png",number:6},
    {color:"./images/pique.png",number:7},
    {color:"./images/pique.png",number:8},
    {color:"./images/pique.png",number:9},
    {color:"./images/pique.png",number:10},
    {color:"./images/pique.png",number:"V"},
    {color:"./images/pique.png",number:"D"},
    {color:"./images/pique.png",number:"K"},
    {color:"./images/pique.png",number:"A"},
    {color:"./images/joker.png",number:"J"}
]

// const randomColor = () => {
//     min = Math.ceil(1);
//     max = Math.floor(5);
//     let randomColor = Math.floor(Math.random() * (max - min)) + min
//     if (randomColor == 1) {
//         symbolCard.setAttribute("src","./images/carreau.png")
//     }
//     else if (randomColor == 2) {
//         symbolCard.setAttribute("src","./images/coeur.png")
//     }
//     else if (randomColor == 3) {
//         symbolCard.setAttribute("src","./images/pique.png")
//     }
//     else if (randomColor == 4) {
//         symbolCard.setAttribute("src","./images/trefle.png")
//     }
//     return randomColor
// }
let index =0
const randomCard = () => {
    let chosenCard = ""
    if (availableCards.length > 0) {
        chosenCard = availableCards[Math.floor(Math.random() * availableCards.length)]
    }else{
        symbolCard.setAttribute("src","./images/kekw.gif")
    }
    index = availableCards.indexOf(chosenCard)
    return chosenCard
}

const cardLists = () => {
    // usedSpells.forEach(element => {
    //     let newDiv = document.createElement("p")
    //     newDiv.innerHTML = `${element.number},${element.color}`
    //     list.appendChild(newDiv)
    // });
    list.innerHTML = ""
    for (let index = 0; index < usedSpells.length; index++) {
        const element = usedSpells[index];
        if (element.color == "./images/carreau.png") {
            element.color = "Carreau"
        }
        else if (element.color == "./images/trefle.png") {
            element.color = "Trèfle"
        }
        else if (element.color == "./images/pique.png") {
            element.color = "Pique"
        }
        else if (element.color == "./images/coeur.png") {
            element.color = "Coeur"
        }
        else if (element.color == "./images/joker.png") {
            element.color = "Joker"
        }

        let colorNumber = ""

        if (element.color == "Joker") {
            colorNumber = element.color
        }else if(element.color == "Coeur" || element.color == "Trèfle" || element.color == "Carreau" || element.color == "Pique"){
            colorNumber = element.number  + " de " + element.color
        }else{
            colorNumber =""
        }
        let newDiv = document.createElement("p")
        newDiv.innerHTML = `${colorNumber}`
        list.appendChild(newDiv)
    }
}

const launchSpell = () => {
    const card = randomCard()
    usedSpells.splice(1,0,card)
    if (availableCards.length >= 1) {
        symbolCard.setAttribute("src",card.color)
        numberCard.innerHTML = card.number
    }else{
        symbolCard.setAttribute("src","./images/kekw.gif")
        numberCard.innerHTML = "RUN"
        button.setAttribute("id","hidden")
    }
    sessionStorage.setItem("usedSpells",[JSON.stringify(usedSpells)])
    availableCards.splice(index,1)
    console.log(usedSpells);
    cardLists()
}



button.addEventListener("click",launchSpell)
